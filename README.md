# TaskManager-Django
Simple todo app written in Django framework.

**Review:**

![Peek 2021-08-24 11-29](https://user-images.githubusercontent.com/71011395/130572165-b50487d0-c669-4aa8-9118-6b888c98f724.gif)

## Used:
- python3
- Django3.2.6
- SQlite datebase

## How to run:
1. clone the project
2. go to project directory ``` cd TaskManager-Django ```  
3. make sure you have python3, pip and virtualenv installed in your machine
4. create virtualenv : ``` python -m venv venv ```  
6. active virtualenv: on mac & linux: ``` source venv/bin/activate ```, on windows : ``` venv\scripts\activate ```    
8. install requirements : ``` pip install -r requirements.txt ```  
9. to create database tables first : ``` cd taskmanager ``` ->> you should see ```manage.py``` file there.  
10. now run : ``` python manage.py migrate ```
11. to run the server : ``` python manage.py runserver ```
12. finally in your browser enter this address ``` http://127.0.0.1:8000/ ```


