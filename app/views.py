from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from users.models import Tasks
from .forms import TodoForm


@login_required
def index(request):
    form = TodoForm(request.POST)
    if request.method == 'POST':
        if form.is_valid():
            Tasks.objects.create(todo=form.cleaned_data['todo'],
                                 status=False, user=request.user)
            form = TodoForm()
    incomplete = Tasks.objects.filter(status=False, user=request.user)
    completed = Tasks.objects.filter(status=True, user=request.user)
    context = {
        'form':form,
        'todo': incomplete,
        'done': completed
    }
    return render(request, 'index.html', context)


@login_required
def complete_task(request, id):
    todo = Tasks.objects.filter(id=id).first()
    todo.status = True
    todo.save()
    return redirect('home')


@login_required
def undo_task(request, id):
    todo = Tasks.objects.filter(id=id).first()
    todo.status = False
    todo.save()
    return redirect('home')


@login_required
def delete_task(request, id):
    todo = Tasks.objects.filter(id=id).first().delete()
    return redirect('home')


@login_required
def delete_completed_tasks(request):
    Tasks.objects.filter(status=True).all().delete()
    return redirect('home')
