from django import forms


class TodoForm(forms.Form):
    todo = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'myclass', 'placeholder': 'Press Enter To Add...'}
            ))