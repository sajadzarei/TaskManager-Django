from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


class Tasks(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    todo = models.CharField(max_length=150)
    status = models.BooleanField(default=False)
    date_created = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f'{self.user.username} task'

    def save(self, *args, **kwargs):
        super(Tasks, self).save(*args, **kwargs)
