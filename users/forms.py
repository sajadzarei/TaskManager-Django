from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User


class UserRegisterForm(UserCreationForm):
    username = forms.CharField(
        widget=forms.TextInput(
           attrs={'class': 'form-field',
                  'placeholder': 'Enter Username...'}
            )
    )
    email = forms.EmailField(
        widget=forms.TextInput(
           attrs={'class': 'form-field',
                  'placeholder': 'Enter Email...'}
            )
    )
    password1 = forms.CharField(
        label='Password',
        widget=forms.PasswordInput(
            attrs={'class': 'form-field',
                   'placeholder': 'Enter Password...'}
        )
    )
    password2 = forms.CharField(
        label='Confirm Password',
        widget=forms.PasswordInput(
            attrs={'class': 'form-field',
                   'placeholder': 'Enter Password Again...'}
        )
    )

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']


class UserLoginForm(AuthenticationForm):
    username = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-field', 'placeholder': 'Enter Username...'}))
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={
            'class': 'form-field',
            'placeholder': 'Enter Password...',
            }
        )
    )

    def __init__(self, *args, **kwargs):
        super(UserLoginForm, self).__init__(*args, **kwargs)
