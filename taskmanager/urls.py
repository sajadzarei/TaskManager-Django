from django.contrib import admin
from django.urls import path
from django.contrib.auth import views as auth_views

from app.views import index, complete_task, delete_completed_tasks,\
    undo_task, delete_task
from users.forms import UserLoginForm
from users import views as user_views


urlpatterns = [
    path('', index, name='home'),
    path('task/done/<int:id>/', complete_task, name='complete_task'),
    path('delete-all/', delete_completed_tasks, name='delete-all'),
    path('delete-task/<int:id>/', delete_task, name='delete-task'),
    path('task/undo/<int:id>/', undo_task, name='undo'),
    path('admin/', admin.site.urls),
    path('register/', user_views.register, name='register'),
    path('login/', auth_views.LoginView.as_view(
        template_name='login.html', authentication_form=UserLoginForm), name='login'),
    path('logout/', auth_views.LogoutView.as_view(
        template_name='logout.html'), name='logout'),

]